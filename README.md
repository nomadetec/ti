# Tools Installer

Instale e configure, rapidamente, ferramentas em plataformas Linux

Desenvolvido em [Shell Script](http://aurelio.net/shell/) e [Dialog](http://aurelio.net/shell/dialog/).

[https://nomadetec.gitlab.io/ti/](https://nomadetec.gitlab.io/ti/)

## Ambiente de desenvolvimento

### Instale o gerenciador de versões Ruby [rbenv](https://rbenv.org/)
    curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash

    ~/.rbenv/bin/rbenv init

    echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >> ~/.bash_profile

### Instale o Ruby
    RUBY_VERSION=$(cat .ruby-version)

    rbenv install $RUBY_VERSION

### Instale as bibliotecas ruby
    bundle install

### Execute o servidor local
    ./localserver.sh

### Baixe o projeto e o copie para um servidor web. Execute:
    SERVIDOR="<IP-DO-SERVIDOR>:4000"

    curl -sS $SERVIDOR/scripts/menu/linux.sh | sudo _CENTRAL_URL_TOOLS=$SERVIDOR bash

## Provisionamento automático

As receitas de instalação estão disponíveis em scripts/recipes. Basta você criar um arquivo recipe.ti e executar o mesmo comando acima para as ferramentas serem instaladas e configuradas automaticamente.

## Licença

Tools Installer é liberado sob a [MIT License](http://www.opensource.org/licenses/MIT).
