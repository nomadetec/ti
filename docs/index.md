---
# Tools Installer

## Instale e configure, rapidamente, ferramentas nas plataformas Linux
![Linux support](images/linux_support.png)

## Ferramentas
* Linguagens de programação
  * [Java](https://www.oracle.com/java/index.html)
  * [Ruby](https://www.ruby-lang.org)
* Servidor web
  * [Nginx](https://www.nginx.com)
* Servidores de aplicação
  * [Puma Service](http://puma.io)
* Servidores de banco de dados
  * [MySQL](https://www.mysql.com)
  * [PostgreSQL](https://www.postgresql.org)
* Gestão de projetos
  * [Redmine](http://www.redmine.org)
* Gerenciador de repositórios de código
  * [GitLab](https://about.gitlab.com)
* Servidor de automação
  * [Jenkins](https://jenkins.io)
* Qualidade de código
  * [SonarQube](http://www.sonarqube.org)
* Gestão de Containers
  * [Docker](https://www.docker.com)
* Cópia de segurança de pastas e banco de dados
  * [Backup](backup)
* Restauração de pastas e banco de dados
  * [Restore](restore)

## É preciso ter instalado os pacotes _curl_, _wget_, _rsync_ e _dialog_

### No Debian ou Ubuntu
    sudo apt-get install -y curl wget rsync dialog

### No CentOS
    sudo yum install -y curl wget rsync dialog

## Provisionamento manual

O comando abaixo inicia o menu principal para instalação das ferramentas

    curl -sS https://nomadetec.gitlab.io/ti/scripts/menu/linux.sh | sudo bash

![manual installer](images/tools-installer-manual.png)

## Provisionamento automático

As receitas de instalação estão disponíveis [no link](https://gitlab.com/nomadetec/ti/-/tree/main/docs/scripts/recipes). Basta você criar um arquivo recipe.ti e executar o mesmo comando acima para as ferramentas serem instaladas e configuradas automaticamente.

### Exemplo de instalação do Jenkins Automation Server

    curl -sS https://nomadetec.gitlab.io/ti/scripts/recipes/jenkins/recipe.ti > recipe.ti

    curl -sS https://nomadetec.gitlab.io/ti/scripts/menu/linux.sh | sudo bash

![automatic installer](images/tools-installer-automatic.png)

[Nômade Tecnologias](https://nomade.tec.br) © 2016
