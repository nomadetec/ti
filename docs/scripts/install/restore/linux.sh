#!/bin/bash
# set -x

# https://aurelio.net/shell/dialog/#itensescolhidos
# http://xmodulo.com/create-dialog-boxes-interactive-shell-script.html

export _APP_NAME="Restore"
_TOOLS_FOLDER="/opt/tools"
_FOLDER="$_TOOLS_FOLDER/backup"
_STORAGE_FOLDER="$_FOLDER/storage"
_CONFIG_FILE="$_FOLDER/backup.conf"
_LOG_FILE="/tmp/restore.log"
_LOG_SYNC="/tmp/synchronizing.log"
_OPTIONS_LIST="perform_sync 'Synchronize server with local folder' \
               select_files 'Select files from restore'"

setup () {
  [ -z "$_CENTRAL_URL_TOOLS" ] && _CENTRAL_URL_TOOLS="https://nomadetec.gitlab.io/ti"

  ping -c 1 "$(echo $_CENTRAL_URL_TOOLS | sed 's|http.*://||g; s|/ti||g;' | cut -d: -f1)" > /dev/null
  [ $? -ne 0 ] && echo "$_CENTRAL_URL_TOOLS connection was not successful!" && exit 1

  _FUNCTIONS_FILE="/tmp/.tools.installer.functions.linux.sh"

  curl -sS $_CENTRAL_URL_TOOLS/scripts/functions/linux.sh > $_FUNCTIONS_FILE 2> /dev/null
  [ $? -ne 0 ] && echo "Functions were not loaded!" && exit 1

  [ -e "$_FUNCTIONS_FILE" ] && source $_FUNCTIONS_FILE && rm $_FUNCTIONS_FILE

  os_check
}

msg_log () {
  _MSG=$1

  echo "" >> "$_LOG_FILE"
  echo "$_MSG" >> "$_LOG_FILE"
  echo "" >> "$_LOG_FILE"

  print_colorful yellow bold "$_MSG"
}

sync_rsync () {
  tool_check rsync

  _HOST_ADDRESS=$(echo $_SYNC_HOST | cut -d: -f1)
  _HOST_PATH=$(echo $_SYNC_HOST | cut -d'|' -f1)
  _HOST_PORT=$(echo $_SYNC_HOST | cut -d'|' -f2)

  run_as_user "$_USER_LOGGED" "ssh -p $_HOST_PORT $_HOST_ADDRESS ls > /dev/null"

  [ $? -ne 0 ] && message "Error" "ssh connect to $_HOST_ADDRESS:$_HOST_PORT. Connection refused"

  [ ! -e "$_FOLDER" ] && mkdir -p $_FOLDER && chown $_USER_LOGGED: -R $_FOLDER

  run_as_user "$_USER_LOGGED" "rsync -CzpOurv --log-file=$_LOG_SYNC --rsh=\"ssh -p$_HOST_PORT\" $_HOST_PATH/backup $_TOOLS_FOLDER >> /dev/null 2>> $_LOG_SYNC"

  [ -e "$_FOLDER/backup.sh" ] && chmod +x "$_FOLDER/backup.sh"

  [ $? -eq 0 ] && tailbox $_LOG_SYNC && message "Notice" "Synchronization successfully!"
}

sync_bucket () {
  tool_check awscli

  aws s3 ls "s3://$_SYNC_HOST" > /dev/null

  [ $? -ne 0 ] && message "Alert" "The aws S3 $_SYNC_HOST not found!"

  [ ! -e "$_FOLDER" ] && mkdir -p $_FOLDER && chown $_USER_LOGGED: -R $_FOLDER

  aws s3 sync "s3://$_SYNC_HOST/" $_FOLDER >> $_LOG_SYNC 2>> $_LOG_SYNC

  [ -e "$_FOLDER/backup.sh" ] && chmod +x "$_FOLDER/backup.sh"

  [ $? -eq 0 ] && tailbox $_LOG_SYNC && message "Notice" "Synchronization successfully!"
}

perform_sync () {
   _SYNC_LIST="rsync 'Remote Synchronization' \
               bucket 'AWS Bucket'"

  _SYNC_OPTION=$(menu "Select from" "$_SYNC_LIST")
  [ -z "$_SYNC_OPTION" ] && main

  _SYNC_HOST=$(input_field "[default]" "Enter the host of $_SYNC_OPTION. Format: server:/path|ssh-port")
  [ $? -eq 1 ] && perform_sync
  [ -z "$_SYNC_HOST" ] && message "Alert" "The host of $_SYNC_OPTION can not be blank!"

  confirm "Do you confirm perform sync from $_SYNC_OPTION - $_SYNC_HOST ?"
  [ $? -eq 1 ] && perform_sync

  sync_$_SYNC_OPTION

  perform_sync
}

perform_restore () {
  _command_name=$1
  _destination=$2
  _command_line=$3
  _container_name=$4
  _access="$_HOST_USER@$_HOST_ADDRESS"

  if [ "$_HOST_ADDRESS" = "local" ]; then
    if [ "$_container_name" != "-" ] && [ "$_RESTORE_TYPE" != "folders" ]; then
      docker exec $_container_name bash -c "command -v "$_command_name"" >> /dev/null 2>> "$_LOG_FILE"
    else
      command -v "$_command_name" >> /dev/null 2>> "$_LOG_FILE"
    fi

    if [ $? -ne 0 ]; then
      message "Error" "$_command_name is not installed!"
    else
      cp "$_STORAGE_FOLDER/$_RESTORE_FILE" "$_destination"

      chown $_USER_LOGGED: "$_destination/$_RESTORE_FILE_NAME"

      [ -e "$_destination/$_RESTORE_FILE_NAME" ] && eval "$_command_line" >> "$_LOG_FILE" 2>> "$_LOG_FILE"
    fi
  else
    _check_command=$(ssh -C -p "$_HOST_PORT" "$_access" "command -v $_command_name")

    if [ -z "$_check_command" ]; then
      message "Error" "$_command_name is not installed!"
    else
      scp -C -P "$_HOST_PORT" "$_STORAGE_FOLDER/$_RESTORE_FILE" "$_access:$_destination" >> "$_LOG_FILE" 2>> "$_LOG_FILE"

      if [ $? -eq 0 ]; then
        ssh -C -p "$_HOST_PORT" "$_access" "$_command_line" >> "$_LOG_FILE" 2>> "$_LOG_FILE"
      fi
    fi
  fi
}

restore_folder () {
  _RESTORE_FOLDER=$(echo $_RESTORE_FILE | cut -d/ -f3)
  _RESTORE_FILE_NAME=$(echo $_RESTORE_FILE | cut -d/ -f4)
  _FOLDER_LIST=$(sed "/^folder:$_RESTORE_FOLDER/ !d;" "$_HOST_FILE")

  if [ -n "$_FOLDER_LIST" ]; then
    _name=$(echo "$_FOLDER_LIST" | cut -d: -f2)
    _path=$(echo "$_FOLDER_LIST" | cut -d: -f3)
    _DEST="/tmp/tools/restore"

    msg_log "> Decompressing $_RESTORE_FILE_NAME to $_HOST_ADDRESS"

    _commands="mkdir -p $_DEST && \
               sudo chown $_USER_LOGGED: -R $_DEST && \
               mv /tmp/$_RESTORE_FILE_NAME $_DEST && \
               cd $_DEST && \
               tar -xJf $_RESTORE_FILE_NAME && \
               sudo rsync -rv $_DEST$_path/ $_path/ && \
               sudo chown $_USER_LOGGED: -R $_path/ && \
               sudo rm -rf $_DEST"

    perform_restore "tar" "/tmp" "$_commands"
  fi
}

restore_database () {
  _RESTORE_DB_TYPE=$(echo $_RESTORE_FILE | cut -d/ -f3)
  _RESTORE_DB_NAME=$(echo $_RESTORE_FILE | cut -d/ -f4)
  _RESTORE_FILE_NAME=$(echo $_RESTORE_FILE | cut -d/ -f5)
  _DB_FIELDS=$(sed "/^database:$_RESTORE_DB_TYPE/ !d; /$_RESTORE_DB_NAME/ !d;" "$_HOST_FILE")

  if [ -n "$_DB_FIELDS" ]; then
    _DB_TYPE=$(echo "$_DB_FIELDS" | cut -d: -f2)
    _DB_HOST=$(echo "$_DB_FIELDS" | cut -d: -f3)
    _DB_USER=$(echo "$_DB_FIELDS" | cut -d: -f4)
    _DB_PASS=$(echo "$_DB_FIELDS" | cut -d: -f5)
    _CONTAINER_NAME=$(echo "$_DB_FIELDS" | cut -d: -f7)
    [ -z "$_CONTAINER_NAME" ] && _CONTAINER_NAME="-"

    _SQL_FILE="${_RESTORE_FILE_NAME/.xz/}"

    _DB_ADMIN_PASS=$(input_field "[default]" "Enter the $_DB_TYPE database administrator password")
    [ $? -eq 1 ] && select_files
    [ -z "$_DB_ADMIN_PASS" ] && message "Alert" "The $_DB_TYPE database administrator password can not be blank!"

    case $_DB_TYPE in
      mysql)
        _COMMAND=mysql
        _DB_COMMAND="$_COMMAND -h $_DB_HOST -u root -p$_DB_ADMIN_PASS -e"
        _CHECK_DB_EXIST="$_DB_COMMAND \"SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$_RESTORE_DB_NAME';\""
        _DB_SQL="$_DB_COMMAND \"CREATE DATABASE $_RESTORE_DB_NAME;\" && "
        _DB_SQL+="$_DB_COMMAND \"CREATE OR REPLACE USER '$_DB_USER'@'$_DB_HOST' IDENTIFIED BY '$_DB_PASS';\" && "
        _DB_SQL+="$_DB_COMMAND \"GRANT ALL PRIVILEGES ON $_RESTORE_DB_NAME.* TO '$_DB_USER'@'$_DB_HOST' WITH GRANT OPTION;\" && "
        _DB_SQL+="$_DB_COMMAND \"FLUSH PRIVILEGES;\""
        _DB_SETUP=$_DB_SQL
        _DB_COPY_FILE=$_SQL_FILE
        _DB_RESTORE="bash -c \"$_COMMAND -h $_DB_HOST -u $_DB_USER -p$_DB_PASS $_RESTORE_DB_NAME < /tmp/$_SQL_FILE\""
        ;;

      postgresql)
        _COMMAND=psql
        _DB_ADMIN_CONNECTION="$_DB_TYPE://postgres:$_DB_ADMIN_PASS@$_DB_HOST"
        _DB_COMMAND="$_COMMAND \"$_DB_ADMIN_CONNECTION\""
        _CHECK_DB_EXIST="$_DB_COMMAND -XtAc \"SELECT 1 FROM pg_database WHERE datname = '$_RESTORE_DB_NAME';\""
        _DB_SQL="$_DB_COMMAND -c \"CREATE ROLE $_DB_USER LOGIN ENCRYPTED PASSWORD '$_DB_PASS' NOINHERIT VALID UNTIL 'infinity';\" && "
        _DB_SQL+="$_DB_COMMAND -c \"CREATE DATABASE $_RESTORE_DB_NAME WITH OWNER=$_DB_USER ENCODING='UTF8';\" && "
        _DB_SQL+="$_COMMAND \"$_DB_ADMIN_CONNECTION/$_RESTORE_DB_NAME\" -c \"GRANT ALL ON SCHEMA public TO $_DB_USER;\""
        _DB_SETUP=$_DB_SQL
        _DB_COPY_FILE=$_SQL_FILE
        _DB_RESTORE="bash -c \"$_COMMAND \"$_DB_TYPE://$_DB_USER:$_DB_PASS@$_DB_HOST/$_RESTORE_DB_NAME\" < /tmp/$_SQL_FILE\""
        ;;

      mongodb)
        _COMMAND=mongosh
        [ -n "$_DB_USER" ] && [ -n "$_DB_PASS" ] && _AUTH="$_DB_USER:$_DB_PASS@"
        _CHECK_DB_EXIST=$(echo "$_COMMAND --eval db.getMongo().getDBNames().indexOf("'"'"$_RESTORE_DB_NAME"'"'")")
        _DB_COPY_FILE=$_RESTORE_DB_NAME
        _DB_RESTORE="mongorestore --uri="'"'"mongodb://$_AUTH$_DB_HOST"'"'" -d $_RESTORE_DB_NAME /tmp/$_RESTORE_DB_NAME"
        ;;
    esac

    if [ "$_CONTAINER_NAME" != "-" ]; then
      _CHECK_DB_EXIST="docker exec $_CONTAINER_NAME $_CHECK_DB_EXIST"
      _DB_COPY="docker cp /tmp/$_DB_COPY_FILE $_CONTAINER_NAME:/tmp"
      _DB_RESTORE="docker exec $_CONTAINER_NAME $_DB_RESTORE"

      case $_DB_TYPE in
        mysql)
          _DB_SETUP=$(echo $_DB_SETUP | sed "s|$_COMMAND -h|docker exec $_CONTAINER_NAME $_COMMAND -h|g")
          ;;
        postgresql)
          _DB_SETUP=$(echo $_DB_SETUP | sed "s|$_COMMAND \"postgresql|docker exec $_CONTAINER_NAME $_COMMAND \"postgresql|g")
        ;;
      esac
    fi

    _DB_EXIST=no

    case $_DB_TYPE in
      mysql)
        eval "$_CHECK_DB_EXIST" > /tmp/.db-exist 2>> "$_LOG_FILE"
        _RESULT=$(grep $_RESTORE_DB_NAME /tmp/.db-exist)
        [ -n "$_RESULT" ] && _DB_EXIST=yes
        ;;

      postgresql)
        eval "$_CHECK_DB_EXIST" > /tmp/.db-exist 2>> "$_LOG_FILE"
        _RESULT=$(cat /tmp/.db-exist)
        [ "$_RESULT" = "1" ] && _DB_EXIST=yes
        ;;

      mongodb)
        _RESULT=$($_CHECK_DB_EXIST)
        [ "$_RESULT" != "-1" ] && _DB_EXIST=yes
        ;;
    esac

    [ "$_DB_EXIST" = "yes" ] && message "Alert" "The $_RESTORE_DB_NAME database already exists"

    msg_log "> Restoring $_DB_TYPE $_RESTORE_DB_NAME database to $_HOST_ADDRESS"

    if [ "$_DB_TYPE" = "mongodb" ]; then
      _commands="tar -C /tmp -xJf /tmp/$_RESTORE_FILE_NAME && \
                rm /tmp/$_RESTORE_FILE_NAME && \
                mv /tmp/tmp/mongodump/$_RESTORE_DB_NAME /tmp && \
                rm -r /tmp/tmp && \
                $_DB_COPY && \
                rm -r /tmp/$_RESTORE_DB_NAME && \
                $_DB_RESTORE"
    else
      _commands="unxz /tmp/$_RESTORE_FILE_NAME && \
                $_DB_COPY && \
                rm /tmp/$_SQL_FILE && \
                $_DB_SETUP && \
                $_DB_RESTORE"

    fi

    perform_restore "$_COMMAND" "/tmp" "$_commands" "$_CONTAINER_NAME"
  fi
}

select_host () {
  _RESTORE_FILE=$1
  _RESTORE_HOST=$(echo $_RESTORE_FILE | cut -d/ -f1)
  _RESTORE_TYPE=$(echo $_RESTORE_FILE | cut -d/ -f2)

  _HOSTS_LIST="$_FOLDER/hosts.list"

  if [ -e "$_HOSTS_LIST" ]; then
    _HOST_FIELDS=$(egrep "^$_RESTORE_HOST" "$_HOSTS_LIST")

    if [ -n "$_HOST_FIELDS" ]; then
      _HOST_NAME=$(echo "$_HOST_FIELDS" | cut -d: -f1)
      _HOST_ADDRESS=$(echo "$_HOST_FIELDS" | cut -d: -f2)
      _HOST_PORT=$(echo "$_HOST_FIELDS" | cut -d: -f3)
      _HOST_USER=$(echo "$_HOST_FIELDS" | cut -d: -f4)

      _HOST_FILE="$_FOLDER/hosts/$_HOST_NAME.list"
      _HOST_FOLDER="$_FOLDER/storage/$_HOST_NAME"

      if [ -e "$_HOST_FILE" ]; then
        case $_RESTORE_TYPE in
          databases)
            restore_database
            ;;
          folders)
            restore_folder
            ;;
        esac
      fi
    fi
  fi
}

select_files () {
  _LIST=""

  for _list in $(ls -R $_STORAGE_FOLDER | grep :)
  do
    _dir=$(echo $_list | cut -d: -f1)

    _file=$(ls -r $_dir/*.xz 2> /dev/null | head -n 1)

    [ -n "$_file" ] && _LIST+="${_file/$_STORAGE_FOLDER\//} '' off "
  done

  if [ -z "$_LIST" ]; then
    message "Alert" "Files from restore not found!" && main
  else
    _LIST+="3>&1 1>&2 2>&3"
  fi

  _FILES=$(checklist "Restore" "What files?" "$_LIST")

  [ -z "$_FILES" ] && main

  confirm "Do you confirm restore from selected items?"
  [ $? -eq 1 ] && main

  [ -e "$_LOG_FILE" ] && rm "$_LOG_FILE"

  msg_log "[$(date +"%Y-%m-%d %H:%M:%S")] Start restore"

  for _file in $_FILES
  do
    select_host $_file
  done

  if [ -e "$_LOG_FILE" ]; then
    _RESTORE_LOG="$_FOLDER/logs/restore.log"

    cat "$_LOG_FILE" >> $_RESTORE_LOG
    chown $_USER_LOGGED: $_RESTORE_LOG
  fi

  textbox $_LOG_FILE

  [ -e "$_LOG_FILE" ] && rm "$_LOG_FILE"

  main
}

main () {
  _USER_LOGGED=$(run_as_root "echo $SUDO_USER")

  if [ "$(provisioning)" = "manual" ]; then
    tool_check dialog

    _OPTION=$(menu "Select the option" "$_OPTIONS_LIST")

    if [ -z "$_OPTION" ]; then
      clear && exit 0
    else
      $_OPTION
    fi
  fi
}

setup
main
